﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WemaPostCardBase
{
    public class PostCardDataDAO : CoreDAO<PostCardData, long>
    {
        public static int InsertPostCardData(PostCardData pcData)
        {

            string conString = ConfigurationManager.ConnectionStrings["VersionDB"].ConnectionString;

            MSSQLDataAccess db = new MSSQLDataAccess(false, conString);

            db.AddParamAndValue("@pan", pcData.pan);
            db.AddParamAndValue("@seq_nr", pcData.seq_nr);
            db.AddParamAndValue("@account_number", pcData.account_number);
            db.AddParamAndValue("@account_type", pcData.account_type);
            db.AddParamAndValue("@name_on_card", pcData.name_on_card);
            db.AddParamAndValue("@lastname", pcData.lastname);
            db.AddParamAndValue("@othernames", pcData.othernames);
            db.AddParamAndValue("@address", pcData.address);
            db.AddParamAndValue("@phone_number", pcData.phone_number);
            db.AddParamAndValue("@downloaded", pcData.downloaded);
            db.AddParamAndValue("@date_uploaded", pcData.date_uploaded);
            db.AddParamAndValue("@username", pcData.username);            

            int status = db.ExecuteNonQuery("sp_insert_post_card_data", CommandType.StoredProcedure);

            db.ClosedbConnection();
            return status;
        }

        public static int UpdatePostCardData(PostCardData pcData)
        {

            string conString = ConfigurationManager.ConnectionStrings["VersionDB"].ConnectionString;

            MSSQLDataAccess db = new MSSQLDataAccess(false, conString);

            db.AddParamAndValue("@pan", pcData.pan);
            db.AddParamAndValue("@seq_nr", pcData.seq_nr);
            db.AddParamAndValue("@account_number", pcData.account_number);
            db.AddParamAndValue("@account_type", pcData.account_type);
            db.AddParamAndValue("@name_on_card", pcData.name_on_card);
            db.AddParamAndValue("@lastname", pcData.lastname);
            db.AddParamAndValue("@othernames", pcData.othernames);
            db.AddParamAndValue("@address", pcData.address);
            db.AddParamAndValue("@phone_number", pcData.phone_number);
            db.AddParamAndValue("@downloaded", pcData.downloaded);
            db.AddParamAndValue("@date_uploaded", pcData.date_uploaded);
            db.AddParamAndValue("@username", pcData.username);
            db.AddParamAndValue("@Srno", pcData.Srno);

            int status = db.ExecuteNonQuery("sp_update_post_card_data", CommandType.StoredProcedure);

            db.ClosedbConnection();
            return status;
        }
    }
}
