﻿using PANE.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WemaPostCardBase
{
    public class PostCardData : DataObject
    {
        public string pan { get; set; }

        public string seq_nr { get; set; }

        public string account_number { get; set; }

        public string account_type { get; set; }

        public string name_on_card { get; set; }

        public string lastname { get; set; }

        public string othernames { get; set; }

        public string address { get; set; }

        public string phone_number { get; set; }

        public bool downloaded { get; set; }

        public DateTime date_uploaded { get; set; }

        public string username { get; set; }
    }
}
