﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;
using System.Security.Permissions;
using System.DirectoryServices.AccountManagement;
using WemaPostCardBase;
using System.Text;

namespace WPCService
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Logout"] == "Y_AD")
                {
                    HttpContext.Current.Session["AdminUser"] = null;
                    txtPassword.Text = "";
                    txtUsername.Text = "";                    
                    MessageBox("Login", "alert('You have successfully logged out');");
                }
                else if (Request.QueryString["Logout"] == "Y_R")
                {
                    HttpContext.Current.Session["RegularUser"] = null;
                    txtPassword.Text = "";
                    txtUsername.Text = "";                    
                    MessageBox("Login", "alert('You have successfully logged out');");
                }
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {            
            try
            {
                string password = txtPassword.Text;
                string username = txtUsername.Text;
                User theUser = new UserSystem().AuthenticateUser(username, password);
                if (theUser != null)
                {

                    if (theUser.UserRole == "Admin User")
                    {
                        HttpContext.Current.Session["AdminUser"] = theUser;
                        Response.Redirect("AdminLanding.aspx");
                    }
                    else
                    {
                        HttpContext.Current.Session["RegularUser"] = theUser;
                        Response.Redirect("RegularLanding.aspx");
                    }
                }
                else
                {
                    MessageBox("Login", "alert('Invalid username or password');");
                }                             
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                new PANE.ERRORLOG.Error().LogToFile(ex);
                MessageBox("Login", "alert('Login was not Successful');");
            }
        }

        public void MessageBox(string key, string script)
        {
            string enclosed = ProcessMsg(script);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), key, enclosed, true);
        }

        private string ProcessMsg(string str)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("function r(f){/in/.test(document.readyState)?setTimeout('r('+f+')',9):f()} r(function(){")
                .Append(str)
                .Append("});");
            return sb.ToString();
        }
    }
}