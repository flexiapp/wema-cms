﻿using PANE.Framework.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WemaPostCardBase
{
    public class User : DataObject
    {
        public virtual string Lastname { get; set; }
        public virtual string Othernames { get; set; }
        public virtual string Email { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual string UserRole { get; set; }
    }
}
