﻿using PANE.Framework.DTO.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WemaPostCardBase.Common.MappingFile
{
    public class UserMap : DataObjectMap<User>
    {
        public UserMap()
        {
            Map(x => x.Lastname);
            Map(x => x.Othernames);
            Map(x => x.Email);
            Map(x => x.Username);
            Map(x => x.Password);
            Map(x => x.UserRole);
        }
    }
}
