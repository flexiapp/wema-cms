﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WemaPostCardBase;

namespace WPCService
{
    public partial class SiteMaster : MasterPage
    {
              
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["AdminUser"] != null)
            {
                User theUser = HttpContext.Current.Session["AdminUser"] as User;
                lblUser.Text = theUser.Lastname + " " + theUser.Othernames + ", ";
            }
            else
            {
                Response.Redirect("Login.aspx");
            }    
        }
    }

}