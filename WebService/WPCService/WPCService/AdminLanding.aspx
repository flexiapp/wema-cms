﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminLanding.aspx.cs" Inherits="WPCService.AdminLanding" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="title">
    <div class="container">
      <div class="row-fluid">
        <div class="span6">
          <h1>Dashboard</h1>
        </div>       
      </div>
    </div>
  </section>
<section class="services">
        <div class="container">
            <div class="row-fluid">
                <div class="span4">
                    <div class="center">
                        <i style="font-size: 48px" class="icon-bar-chart icon-large"></i>
                        <p> </p>
                        <h4>USER MANAGEMENT</h4>
                       <p>Add, View and Update Users</p>
                        <a href="UserManagement.aspx" style="text-decoration:underline" title="Manage User">Manage User</a>
                    </div>
                </div>

                <div class="span4">
                    <div class="center">
                        <i style="font-size: 48px" class="icon-cog icon-large"></i>
                        <p> </p>
                        <h4>VERSION MANAGEMENT</h4>
                         <p>Add, Modify and View Version Updates</p>
                        <a href="VersionManagementAdmin.aspx" style="text-decoration:underline" title="Manage Version Updates">Manage Version Updates</a>
                    </div>
                </div>

                <div class="span4">
                    <div class="center">
                        <i style="font-size: 48px" class="icon-heart icon-large"></i>
                        <p> </p>
                        <h4>PASSWORD MANAGEMENT</h4>
                        <p>Change Password</p>
                        <a href="AdminPasswordManagement.aspx" style="text-decoration:underline" title="Change Password">Change Password</a>
                    </div>
                </div>

            </div>

            <hr style="border-width:2px; border-style:inset;"/>                      
            <p>&nbsp;</p>

        </div>
    </section>
    </asp:Content>
