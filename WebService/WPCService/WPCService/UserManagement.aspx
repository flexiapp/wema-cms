﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="WPCService.UserManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <section class="title">
    <div class="container">
      <div class="row-fluid">
        <div class="span6">
          <h1>User Management</h1>
        </div>
        <div class="span6">
          <ul class="breadcrumb pull-right">
            <li><a href="adminLanding.aspx">Dashboard</a> <span class="divider">/</span></li>
            <li><a href="UserManagement.aspx">User Management</a></li>            
          </ul>
        </div>
      </div>
    </div>
  </section>
 <section class="services">
        <div class="container">            
        <asp:UpdatePanel ID="updPanl" runat="server" RenderMode="Block" UpdateMode="Conditional" >
        <ContentTemplate>
        <div class="what-we-do container">
            <div class="row">
                <div class="team-text span12">
                   
                   <h4 style="text-align:left; color:Black">Add User</h4>
                   <hr style="border-style:groove" />
                   <table cellpadding="4px" cellspacing="4px">
                   <tr>
                   <td style="width:150px; text-align:left"></td>
                   <td><asp:Label runat="server" ID="lblmsg" ForeColor="Red" Font-Italic="true"></asp:Label></td>
                   </tr>
                   <tr>
                   <td style="color:Black;text-align:left">Surname:</td>
                   <td><asp:TextBox runat="server" ID="txtLastname" Width="350px"></asp:TextBox></td>
                     <td><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ErrorMessage="Required" Font-Italic="True" Font-Names="Calibri" 
                        ControlToValidate="txtLastname" ForeColor="Red">Required</asp:RequiredFieldValidator>
                        <br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                        ControlToValidate="txtLastname" ErrorMessage="Invalid Name" Font-Bold="False" 
                        Font-Italic="True" Font-Names="calibri" Font-Size="Small" 
                        ValidationExpression="\D*" ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                   </tr>
                   <tr>
                   <td style="color:Black;text-align:left">Othernames:</td>
                   <td><asp:TextBox runat="server" ID="txtOthernames" Width="350px"></asp:TextBox></td>
                   <td><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ErrorMessage="Required" Font-Italic="True" Font-Names="Calibri" 
                        ControlToValidate="txtOthernames" ForeColor="Red">Required</asp:RequiredFieldValidator>                                           
                        </td>
                   </tr>
                   <tr>
                   <td style="color:Black;text-align:left">Email:</td>
                   <td><asp:TextBox runat="server" ID="txtEmail" Width="350px"></asp:TextBox></td>
                   <td> <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ControlToValidate="txtEmail" ErrorMessage="Invalid Email" Font-Italic="True" 
                        Font-Names="Calibri" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                        ForeColor="Red"></asp:RegularExpressionValidator>
                        </td>
                   </tr>                                     
                    <tr>
                   <td style="color:Black;text-align:left">Role:</td>
                   <td>
                    <asp:DropDownList ID="comboRole" runat="server" Width="385px">
                     <asp:ListItem Enabled="true" Value="0">--select role--</asp:ListItem>  
                     <asp:ListItem Enabled="true" Value="Admin User">Admin User</asp:ListItem>  
                     <asp:ListItem Enabled="true" Value="Regular User">Regular User</asp:ListItem>  
                    </asp:DropDownList>                   
                    </td>
                    <td><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ErrorMessage="Required" Font-Italic="True" Font-Names="Calibri" 
                            ControlToValidate="comboRole" ForeColor="Red" InitialValue="0">Required</asp:RequiredFieldValidator></td>
                   </tr>
                    <tr>
                   <td style="color:Black;text-align:left">Username:</td>
                   <td><asp:TextBox runat="server" ID="txtUsername" Width="350px"></asp:TextBox></td>
                     <td><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ErrorMessage="Required" Font-Italic="True" Font-Names="Calibri" 
                        ControlToValidate="txtUsername" ForeColor="Red">Required</asp:RequiredFieldValidator>                                           
                        </td>
                   </tr>                  
                   <tr>
                   <td></td>
                   <td style="text-align:right">                               
                   <asp:Button runat="server" ID="btnAdd" Text="Add" Width="100px" CssClass="btn btn-primary" 
                          BackColor="Green" ForeColor="White"  onclick="btnAdd_Click" /></td>
                   </tr>
                     <tr>
                   <td></td>
                   <td> <asp:UpdateProgress ID="UPG1" runat="server" AssociatedUpdatePanelID="updPanl"  Visible="true"  >
                        <ProgressTemplate >
                            <p style="color:Green">Processing... Please wait.</p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>  </td>
                   </tr>
                   </table>  
                   <hr style="border-style:groove" />
                   <h4 style="text-align:left">View Users</h4>
                   <hr style="border-style:groove" />
                    <div>
                    
                        <asp:GridView ID="gvUsers" runat="server" AllowPaging="True" onpageindexchanging="GridView1_PageIndexChanging" 
                        CssClass="table table-hover"  AutoGenerateColumns="False"  PageSize="20" 
                        PagerSettings-Mode="NextPrevious" Width="100%" EmptyDataText="There are no <b>Result(s)</b> matching search criteria">
                           <Columns>                                                                        
                        <asp:TemplateField HeaderText="Surname"  HeaderStyle-BackColor="LightGreen" HeaderStyle-BorderStyle="Outset" HeaderStyle-ForeColor="Black" HeaderStyle-BorderColor="Black"  >
                            <ItemTemplate>
                                <asp:Label ID="lblSurname" runat="server" Text='<%# Eval("Lastname") %>' />
                            </ItemTemplate>                            
                            <ItemStyle ForeColor="Black"  />
                            <HeaderStyle  HorizontalAlign="Left" />
                        </asp:TemplateField>   
                        
                        <asp:TemplateField HeaderText="Other Names"  HeaderStyle-BackColor="LightGreen" HeaderStyle-BorderStyle="Outset" HeaderStyle-ForeColor="Black" HeaderStyle-BorderColor="Black" >
                            <ItemTemplate>
                                <asp:Label ID="lblOthernames" runat="server" Text='<%# Eval("Othernames") %>' />
                            </ItemTemplate>                            
                            <ItemStyle  ForeColor="Black"  />
                            <HeaderStyle  HorizontalAlign="Left" />
                        </asp:TemplateField>   

                        <asp:TemplateField HeaderText="Email"  HeaderStyle-BackColor="LightGreen" HeaderStyle-BorderStyle="Outset" HeaderStyle-ForeColor="Black" HeaderStyle-BorderColor="Black" >
                            <ItemTemplate>
                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>' />
                            </ItemTemplate>                            
                            <ItemStyle  ForeColor="Black"  />
                            <HeaderStyle  HorizontalAlign="Left" />
                        </asp:TemplateField>                         
                          <asp:TemplateField HeaderText="Username"  HeaderStyle-BackColor="LightGreen" HeaderStyle-BorderStyle="Outset" HeaderStyle-ForeColor="Black" HeaderStyle-BorderColor="Black" >
                            <ItemTemplate>
                                <asp:Label ID="lblUsername" runat="server" Text='<%# Eval("Username") %>' />
                            </ItemTemplate>                            
                            <ItemStyle  ForeColor="Black"  />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Role"  HeaderStyle-BackColor="LightGreen" HeaderStyle-BorderStyle="Outset" HeaderStyle-ForeColor="Black" HeaderStyle-BorderColor="Black" >
                            <ItemTemplate>
                                <asp:Label ID="lblUserRole" runat="server" Text='<%# Eval("UserRole") %>' />
                            </ItemTemplate>                            
                            <ItemStyle  ForeColor="Black"  />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                          <asp:TemplateField HeaderStyle-BackColor="LightGreen" HeaderStyle-BorderStyle="Outset" HeaderStyle-ForeColor="Black" HeaderStyle-BorderColor="Black" >
                            <ItemTemplate >
                                <asp:HyperLink ID="hlLicenseInfo" runat="server" Font-Underline="true"                                                                    
                                    NavigateUrl='<%# "UserManagement.aspx?ItemID=" + Eval("Srno") %>'>View Details</asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right"  ForeColor="Black" />
                    </asp:TemplateField>                                                                                                                                        
                    </Columns>
                        </asp:GridView>
                    
                    </div>                
                </div>
            </div>
        </div>   
        </ContentTemplate>
        </asp:UpdatePanel>       
           
            <p>&nbsp;</p>

        </div>
    </section>   
    </asp:Content>
