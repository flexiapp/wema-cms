﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WemaPostCardBase;
using PANE.Framework.NHibernateManager;
using System.Web.Security;
using System.Text;

namespace WPCService
{
    public partial class UserManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {              
                if (Request.QueryString != null)
                {
                    long id = Convert.ToInt64(Request.QueryString["ItemID"]);

                    User theUser = new UserSystem().GetUserByID(id);
                    if (theUser != null)
                    {
                        txtLastname.Text = theUser.Lastname;
                        txtOthernames.Text = theUser.Othernames;
                        txtEmail.Text = theUser.Email;
                        txtUsername.Text = theUser.Username;                        
                        comboRole.SelectedValue = theUser.UserRole;
                        btnAdd.Text = "Update";                        
                    }
                }
                else
                {
                    btnAdd.Text = "Add";                
                }
                GetUsers();
            }
        }

        protected void GetUsers()
        {
            List<User> AllUsers = new UserSystem().GetAllUsers();

            gvUsers.DataSource = AllUsers;
            gvUsers.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnAdd.Text == "Add")
                {
                    User theUser = new User();
                    theUser.Lastname = txtLastname.Text.Trim();
                    theUser.Othernames = txtOthernames.Text.Trim();
                    theUser.Email = txtEmail.Text.Trim();
                    theUser.Username = txtUsername.Text.Trim();
                    //theUser.Password = FormsAuthentication.HashPasswordForStoringInConfigFile("password", "MD5");
                    theUser.Password = "password";                    
                    theUser.UserRole = comboRole.SelectedValue.ToString();

                    User UserExists = new UserSystem().GetUserByUsername(theUser.Username);
                    if (UserExists != null)
                    {
                        MessageBox("User Management", "alert('User with username exists already.');");
                    }
                    else
                    {                        
                        if (new UserSystem().Save(theUser))
                        {                            
                            MessageBox("User Management", "alert('User added successfully.');");
                            txtLastname.Text = "";
                            txtOthernames.Text = "";
                            txtEmail.Text = "";
                            txtUsername.Text = "";
                            comboRole.SelectedValue = "0";                            
                            GetUsers();
                        }
                        else
                        {
                            MessageBox("User Management", "alert('Request was not successful.');");
                        }

                    }
                }
                else if (btnAdd.Text == "Update")
                {
                    long id = Convert.ToInt64(Request.QueryString["ItemID"]);

                    User theUser = new UserSystem().GetUserByID(id);
                    theUser.Lastname = txtLastname.Text.Trim();
                    theUser.Othernames = txtOthernames.Text.Trim();
                    theUser.Email = txtEmail.Text.Trim();
                    theUser.Username = txtUsername.Text.Trim();                    
                    theUser.UserRole = comboRole.SelectedValue.ToString();

                    if (new UserSystem().Update(theUser))
                    {                        
                        MessageBox("User Management", "alert('User updated successfully.');");
                        txtLastname.Text = "";
                        txtOthernames.Text = "";
                        txtEmail.Text = "";
                        txtUsername.Text = "";                        
                        comboRole.SelectedValue = "0";                        
                        GetUsers();                       
                        btnAdd.Text = "Add";
                    }
                    else
                    {                        
                        MessageBox("User Management", "alert('Request was not successful.');");
                    }
                }
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
                MessageBox("User Management", "alert('User operation was not Successful');");
            }
            finally
            {
                NHibernateSessionManager.Instance.CloseSession();
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUsers.PageIndex = e.NewPageIndex;
            GetUsers();
        }

        public void MessageBox(string key, string script)
        {
            string enclosed = ProcessMsg(script);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), key, enclosed, true);
        }

        private string ProcessMsg(string str)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("function r(f){/in/.test(document.readyState)?setTimeout('r('+f+')',9):f()} r(function(){")
                .Append(str)
                .Append("});");
            return sb.ToString();
        }
    }
}