﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminPasswordManagement.aspx.cs" Inherits="WPCService.AdminPasswordManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<section class="title">
    <div class="container">
      <div class="row-fluid">
        <div class="span6">
          <h1>Password Management</h1>
        </div>
        <div class="span6">
          <ul class="breadcrumb pull-right">
            <li><a href="adminLanding.aspx">Dashboard</a> <span class="divider">/</span></li>
            <li><a href="AdminPasswordManagement.aspx">Password Management</a></li>            
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section class="services">
        <div class="container">            
             <asp:UpdatePanel ID="updPanl" runat="server" RenderMode="Block" UpdateMode="Conditional" >
        <ContentTemplate>
        <div class="what-we-do container">
            <div class="row">
                <div class="team-text span12">
                   
                   <h4 style="text-align:left; color:Black">Change Password</h4>
                   <hr style="border-style:groove" />
                   <table cellpadding="4px" cellspacing="4px">
                   <tr>
                   <td style="width:150px; text-align:left"></td>
                   <td><asp:Label runat="server" ID="lblmsg" ForeColor="Red" Font-Italic="true"></asp:Label><br />                   
                   </td>
                   </tr>
                   <tr>
                   <td style="color:Black">New Password:</td>
                   <td><asp:TextBox runat="server" ID="txtNewPassword" Width="350px" TextMode="Password"></asp:TextBox></td>
                   </tr>
                   <tr>
                   <td style="color:Black">Confirm Password:</td>
                   <td><asp:TextBox runat="server" ID="txtConfirmPassword" Width="350px" TextMode="Password"></asp:TextBox></td>
                   </tr>
                   <tr>
                   <td></td>
                   <td style="text-align:right">                                   
                   <asp:Button runat="server" ID="btnChange" Text="Change Password" Width="120px" 
                           BackColor="Green" ForeColor="White" CssClass="btn btn-primary" 
                           onclick="btnChange_Click"  /></td>
                   </tr>
                   <tr>
                   <td></td>
                   <td> <asp:UpdateProgress ID="UPG1" runat="server" AssociatedUpdatePanelID="updPanl"  Visible="true"  >
                        <ProgressTemplate >
                            <p style="color:Green">Processing... Please wait.</p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>  </td>
                   </tr>
                   </table>
                   
                   
                   <hr style="border-style:groove" />                 
                </div>
            </div>
        </div>
        </ContentTemplate>
        </asp:UpdatePanel>       
           
            <p>&nbsp;</p>

        </div>
    </section>
</asp:Content>
