﻿using PANE.Framework.DTO.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WemaPostCardBase.Common.MappingFile
{
    public class PostCardDataMap: DataObjectMap<PostCardData>
    {
        public PostCardDataMap()
        {
            Map(x => x.pan);
            Map(x => x.seq_nr);
            Map(x => x.account_number);
            Map(x => x.account_type);
            Map(x => x.name_on_card);
            Map(x => x.lastname);
            Map(x => x.othernames);
            Map(x => x.address);
            Map(x => x.phone_number);
            Map(x => x.downloaded);
            Map(x => x.date_uploaded);
            Map(x => x.username);
        }
    }
}
