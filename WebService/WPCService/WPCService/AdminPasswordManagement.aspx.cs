﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WemaPostCardBase;
using System.Web.Security;
using System.Text;

namespace WPCService
{
    public partial class AdminPasswordManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            if (txtNewPassword.Text != txtConfirmPassword.Text)
            {
                MessageBox("Password Management", "alert('Kindly ensure that the two password fields have the same value.');");
            }
            else if (string.IsNullOrEmpty(txtNewPassword.Text) || string.IsNullOrEmpty(txtConfirmPassword.Text))
            {
                MessageBox("Password Management", "alert('The two password fields are necessary to change password.');");
            }
            else
            {
                User theUser = HttpContext.Current.Session["AdminUser"] as User;
                if (theUser == null)
                {
                    Response.Redirect("Login.aspx");
                }

                theUser.Password = txtNewPassword.Text;

                bool updateUser = new UserSystem().Update(theUser);

                if (updateUser)
                {
                    MessageBox("Password Management", "alert('Password change was successful. Kindly logout and login with your new password');");
                    txtConfirmPassword.Text = "";
                    txtNewPassword.Text = "";
                }
                else
                {
                    MessageBox("Password Management", "alert('Request was not successful.');");
                }

            }
        }

        public void MessageBox(string key, string script)
        {
            string enclosed = ProcessMsg(script);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), key, enclosed, true);
        }

        private string ProcessMsg(string str)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("function r(f){/in/.test(document.readyState)?setTimeout('r('+f+')',9):f()} r(function(){")
                .Append(str)
                .Append("});");
            return sb.ToString();
        }
    }
}