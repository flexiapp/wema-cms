﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WemaPostCardBase
{
    public class UserSystem : CoreServices<User, long>
    {
        public bool Save(User theUser)
        {
            bool result = false;
            try
            {
                int save = UserDAO.InsertUser(theUser);
                if (save > 0)
                    result = true;
            }
            catch (Exception ex)
            {                
                result = false;
                
                throw ex;
            }
            return result;
        }

        public bool Update(User theUser)
        {
            bool result = false;
            try
            {               
                int save = UserDAO.UpdateUser(theUser);
                if (save > 0)
                    result = true;
            }
            catch (Exception ex)
            {                
                result = false;
                
                throw ex;
            }
            return result;
        }

        public List<User> GetAllUsers()
        {
            return new UserDAO().RetrieveAll();
        }

        public User GetUserByID(long id)
        {
            return new UserDAO().Retrieve(id);
        }

        public User AuthenticateUser(string username, string password)
        {
            return new UserDAO().AuthenticateUser(username, password);
        }

        public User GetUserByUsername(string userName)
        {
            return new UserDAO().GetUserByUsername(userName);
        }      
    }
}
