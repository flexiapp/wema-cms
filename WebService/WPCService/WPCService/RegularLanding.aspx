﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RegularSite.Master" AutoEventWireup="true" CodeBehind="RegularLanding.aspx.cs" Inherits="WPCService.RegularLanding" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="title">
    <div class="container">
      <div class="row-fluid">
        <div class="span6">
          <h1>Dashboard</h1>
        </div>       
      </div>
    </div>
  </section>
<section class="services">
        <div class="container">
            <div class="row-fluid">
                <div class="span4">
                    <div class="center">
                        <i style="font-size: 48px" class="icon-bar-chart icon-large"></i>
                        <p> </p>
                        <h4>VERSION MANAGEMENT</h4>
                         <p>Add, Modify and View Version Updates</p>
                        <a href="VersionManagementRegular.aspx" style="text-decoration:underline" title="Manage Version Updates">Manage Version Updates</a>
                    </div>
                </div>              
                
                <div class="span4">
                    <div class="center">
                        <i style="font-size: 48px" class="icon-globe icon-large"></i>
                        <p> </p>
                         <h4>PASSWORD MANAGEMENT</h4>
                        <p>Change Password</p>
                        <a href="RegularPasswordManagement.aspx" style="text-decoration:underline" title="Change Password">Change Password</a>
                    </div>
                </div>
               
            </div>

             <hr style="border-width:2px; border-style:inset;"/>            

            <div class="row-fluid">                
            </div>          

           
            <p>&nbsp;</p>

        </div>
    </section>
</asp:Content>
