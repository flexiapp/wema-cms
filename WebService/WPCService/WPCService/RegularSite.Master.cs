﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WemaPostCardBase;

namespace WPCService
{
    public partial class RegularSite : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["RegularUser"] != null)
            {
                User theUser = HttpContext.Current.Session["RegularUser"] as User;
                lblUser.Text = theUser.Lastname + " " + theUser.Othernames + ", ";
            }
            else
            {
                Response.Redirect("Login.aspx");
            }    
        }
    }
}