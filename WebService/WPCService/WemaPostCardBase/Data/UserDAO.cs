﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using System.Configuration;
using System.Data;

namespace WemaPostCardBase
{
    public class UserDAO : CoreDAO<User, long>
    {
        public User AuthenticateUser(string userName, string Password)
        {
            ISession session = BuildSession();
            ICriteria criteria = session.CreateCriteria(typeof(User));
            criteria.Add(Expression.Eq("Username", userName));
            criteria.Add(Expression.Eq("Password", Password));            
            return criteria.UniqueResult<User>();
        }

        public User GetUserByUsername(string userName)
        {
            ISession session = BuildSession();
            ICriteria criteria = session.CreateCriteria(typeof(User));
            criteria.Add(Expression.Eq("Username", userName));            
            return criteria.UniqueResult<User>();
        }

        public static int InsertUser(User somebody)
        {

            string conString = ConfigurationManager.ConnectionStrings["VersionDB"].ConnectionString;

            MSSQLDataAccess db = new MSSQLDataAccess(false, conString);

            db.AddParamAndValue("@Lastname", somebody.Lastname.ToUpper());
            db.AddParamAndValue("@Othernames", somebody.Othernames.ToUpper());
            db.AddParamAndValue("@Email", somebody.Email);
            db.AddParamAndValue("@Username", somebody.Username);
            db.AddParamAndValue("@UserRole", somebody.UserRole);            
            db.AddParamAndValue("@Password", somebody.Password);

            int status = db.ExecuteNonQuery("sp_insert_user", CommandType.StoredProcedure);

            db.ClosedbConnection();
            return status;
        }

        public static int UpdateUser(User somebody)
        {

            string conString = ConfigurationManager.ConnectionStrings["VersionDB"].ConnectionString;

            MSSQLDataAccess db = new MSSQLDataAccess(false, conString);

            db.AddParamAndValue("@Lastname", somebody.Lastname.ToUpper());
            db.AddParamAndValue("@Othernames", somebody.Othernames.ToUpper());
            db.AddParamAndValue("@Email", somebody.Email);
            db.AddParamAndValue("@Username", somebody.Username);
            db.AddParamAndValue("@UserRole", somebody.UserRole);            
            db.AddParamAndValue("@Password", somebody.Password);
            db.AddParamAndValue("@ID", somebody.Srno);

            int status = db.ExecuteNonQuery("sp_update_user", CommandType.StoredProcedure);

            db.ClosedbConnection();
            return status;
        }
    }
}
