﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WPCService.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login Page</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="robots" content="index, follow" />
	<meta charset="utf-8" />
	<!-- // General meta information -->
		
	<!-- Load Javascript -->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.query-2.1.7.js"></script>
	<script type="text/javascript" src="js/rainbows.js"></script>
	<!-- // Load Javascipt -->

	<!-- Load stylesheets -->
	<link type="text/css" rel="stylesheet" href="css/style.css" media="screen" />
	<!-- // Load stylesheets -->
	
<script type="text/javascript">


    $(document).ready(function () {

        $("#submit1").hover(
	function () {
	    $(this).animate({ "opacity": "0" }, "slow");
	},
	function () {
	    $(this).animate({ "opacity": "1" }, "slow");
	});
    });


</script>
<style type="text/css">
table
{
    padding:10px;
    margin-left:20px;    
}
</style>
</head>
<body>
    <form id="form1" runat="server">   
    <asp:ScriptManager runat="server" ID="Manager1"></asp:ScriptManager> 
    <div id="wrapper">
		<div id="wrappertop"></div>

		<div id="wrappermiddle">

			<h2 style="font-family:Fjalla One;font-size:14px;text-transform:uppercase; color:Black;">Login</h2>            
             <asp:UpdatePanel ID="updPanl" runat="server" RenderMode="Block" UpdateMode="Conditional" >
            <ContentTemplate>
			<table cellpadding="5px" cellspacing="8px">
             <tr><td>
                       &nbsp;&nbsp; &nbsp;</td></tr>
                   <tr>                  
                   <td><asp:Label runat="server" ID="lblmsg" ForeColor="Red" Font-Italic="true"></asp:Label><br />                   
                   </td>
                   </tr>                  
                   <tr>
                   <td style="color:Green">Username:</td>                   
                   </tr                               
                   <tr>
                   <td><asp:TextBox runat="server" ID="txtUsername" Width="280px" Height="30px"></asp:TextBox></td>
                   </tr>
                   <tr><td> &nbsp;&nbsp; &nbsp;</td></tr>
                   <tr>
                   <td style="color:Green">Password:</td>                   
                   </tr>                
                   <tr>
                   <td><asp:TextBox runat="server" ID="txtPassword" Width="280px" Height="30px" TextMode="Password"></asp:TextBox></td>
                   </tr>
                   <tr><td> &nbsp;&nbsp; &nbsp;</td></tr>
                   <tr>                  
                   <td style="text-align:right">                                   
                   <asp:Button runat="server" ID="btnLogin" Text="Login" Width="120px" Height="30px" 
                           BackColor="Green" ForeColor="White" CssClass="btn btn-primary" OnClick="btnLogin_Click" 
                             /></td>
                   </tr>
                   <tr>                   
                   <td> <asp:UpdateProgress ID="UPG1" runat="server" AssociatedUpdatePanelID="updPanl"  Visible="true"  >
                        <ProgressTemplate >
                            <p style="color:Green">Processing... Please wait.</p>
                        </ProgressTemplate>
                    </asp:UpdateProgress>  </td>
                   </tr>
                   </table>
            </ContentTemplate>
        </asp:UpdatePanel>    

			<div id="links_left">
			

			</div>

			<div id="links_right">
            </div>

		</div>

		<div id="wrapperbottom"></div>
		
		<div id="powered">
		<p>Powered by <a href="http://www.pajuno.com">Pajuno Development Company Ltd.</a></p>
		</div>
	</div>

    </form>
</body>
</html>

